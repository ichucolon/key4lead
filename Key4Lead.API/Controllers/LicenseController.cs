using System.Threading.Tasks;
using AutoMapper;
using Key4Lead.API.Data;
using Key4Lead.API.Dtos;
using Key4Lead.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Key4Lead.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LicenseController : ControllerBase
    {
        private readonly IKeyRepo _repo;

        private readonly IMapper _mapper;
        public LicenseController(IKeyRepo repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetLicenses()
        {
            var keys = await _repo.GetLicenses();
            return Ok(keys);
        }

        [HttpGet("{key}")]
        public async Task<IActionResult> GetLicense(string key)
        {
            var Key = await _repo.GetLicense(key);
            var returnkey = _mapper.Map<KeyDto>(Key);
            return Ok(returnkey);
        }

    }
}