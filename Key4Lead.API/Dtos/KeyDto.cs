using System;
using Key4Lead.API.Models;

namespace Key4Lead.API.Dtos
{
    public class KeyDto {
        public bool Expired { get; set; }
        public bool? Flag { get; set; }
        public int CompanyId { get; set; }
        public string EventId { get; set; }

    }
}