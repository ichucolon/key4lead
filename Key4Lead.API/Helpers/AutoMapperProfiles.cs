using AutoMapper;
using Key4Lead.API.Dtos;
using Key4Lead.API.Models;
using System.Linq;

namespace Key4Lead.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<License, KeyDto>()
            .ForMember(dest => dest.Expired, opt => {
                opt.ResolveUsing(d => d.Expiration.Expiry());
            });
        }
    }
}