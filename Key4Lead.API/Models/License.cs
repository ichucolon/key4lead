﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("license")]
    public partial class License
    {
        [Key]
        [Column("licenseKey")]
        [StringLength(100)]
        public string LicenseKey { get; set; }
        [Column("expiration", TypeName = "datetime")]
        public DateTime? Expiration { get; set; }
        [Column("companyId")]
        public int CompanyId { get; set; }
        [Required]
        [Column("eventId")]
        [StringLength(30)]
        public string EventId { get; set; }
        [Column("flag")]
        public bool? Flag { get; set; }

        [ForeignKey("CompanyId")]
        [InverseProperty("License")]
        public Company Company { get; set; }
        [ForeignKey("EventId")]
        [InverseProperty("License")]
        public Event Event { get; set; }
    }
}
