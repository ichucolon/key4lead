﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Key4Lead.API.Models
{
    public partial class key4leadContext : DbContext
    {
        public key4leadContext(DbContextOptions<key4leadContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<FrmAnswer> FrmAnswer { get; set; }
        public virtual DbSet<FrmStruct> FrmStruct { get; set; }
        public virtual DbSet<Lead> Lead { get; set; }
        public virtual DbSet<LeadStruct> LeadStruct { get; set; }
        public virtual DbSet<License> License { get; set; }
        public virtual DbSet<StructEvent> StructEvent { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     if (!optionsBuilder.IsConfigured)
        //     {
        //         optionsBuilder.UseSqlServer("Server=localhost;Database=key4lead;Uid=SA;Pwd=P@ssword01;");
        //     }
        // }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.Contact).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.TimeZone).IsUnicode(false);
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.Property(e => e.EventId)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BarcodeType).IsUnicode(false);

                entity.Property(e => e.Location).IsUnicode(false);

                entity.Property(e => e.PhotoUrl).IsUnicode(false);
            });

            modelBuilder.Entity<FrmAnswer>(entity =>
            {
                entity.Property(e => e.FId).ValueGeneratedNever();

                entity.HasOne(d => d.Fstruct)
                    .WithMany(p => p.FrmAnswer)
                    .HasForeignKey(d => d.FstructId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__frm_answe__fstru__4F7CD00D");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.FrmAnswer)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__frm_answe__leadI__5070F446");
            });

            modelBuilder.Entity<FrmStruct>(entity =>
            {
                entity.Property(e => e.FstructId).ValueGeneratedNever();

                entity.Property(e => e.EventId).IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.FrmStruct)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__frm_struc__compa__4CA06362");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.FrmStruct)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__frm_struc__event__4BAC3F29");
            });

            modelBuilder.Entity<Lead>(entity =>
            {
                entity.Property(e => e.LeadId).ValueGeneratedNever();

                entity.HasOne(d => d.Struct)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.StructId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__lead__structId__48CFD27E");
            });

            modelBuilder.Entity<LeadStruct>(entity =>
            {
                entity.Property(e => e.StructId).ValueGeneratedNever();
            });

            modelBuilder.Entity<License>(entity =>
            {
                entity.Property(e => e.LicenseKey)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.EventId).IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.License)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__license__company__3E52440B");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.License)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__license__eventId__3F466844");
            });

            modelBuilder.Entity<StructEvent>(entity =>
            {
                entity.Property(e => e.SeId).ValueGeneratedNever();

                entity.Property(e => e.EventId).IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.StructEvent)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__struct_ev__compa__45F365D3");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.StructEvent)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__struct_ev__event__44FF419A");

                entity.HasOne(d => d.Struct)
                    .WithMany(p => p.StructEvent)
                    .HasForeignKey(d => d.StructId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__struct_ev__struc__440B1D61");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasIndex(e => e.Username)
                    .HasName("UQ__users__F3DBC5725E030787")
                    .IsUnique();

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__users__companyId__398D8EEE");
            });
        }
    }
}
