﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("struct_event")]
    public partial class StructEvent
    {
        [Key]
        [Column("seId")]
        public int SeId { get; set; }
        [Column("structId")]
        public int StructId { get; set; }
        [Required]
        [Column("eventId")]
        [StringLength(30)]
        public string EventId { get; set; }
        [Column("companyID")]
        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        [InverseProperty("StructEvent")]
        public Company Company { get; set; }
        [ForeignKey("EventId")]
        [InverseProperty("StructEvent")]
        public Event Event { get; set; }
        [ForeignKey("StructId")]
        [InverseProperty("StructEvent")]
        public LeadStruct Struct { get; set; }
    }
}
