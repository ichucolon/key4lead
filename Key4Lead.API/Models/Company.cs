﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("company")]
    public partial class Company
    {
        public Company()
        {
            FrmStruct = new HashSet<FrmStruct>();
            License = new HashSet<License>();
            StructEvent = new HashSet<StructEvent>();
            Users = new HashSet<Users>();
        }

        [Column("companyId")]
        public int CompanyId { get; set; }
        [Required]
        [Column("name")]
        [StringLength(100)]
        public string Name { get; set; }
        [Column("address")]
        [StringLength(100)]
        public string Address { get; set; }
        [Column("contact")]
        [StringLength(30)]
        public string Contact { get; set; }
        [Column("email")]
        [StringLength(30)]
        public string Email { get; set; }
        [Column("displayName")]
        [StringLength(30)]
        public string DisplayName { get; set; }
        [Column("language")]
        [StringLength(30)]
        public string Language { get; set; }
        [Column("timeZone")]
        [StringLength(30)]
        public string TimeZone { get; set; }

        [InverseProperty("Company")]
        public ICollection<FrmStruct> FrmStruct { get; set; }
        [InverseProperty("Company")]
        public ICollection<License> License { get; set; }
        [InverseProperty("Company")]
        public ICollection<StructEvent> StructEvent { get; set; }
        [InverseProperty("Company")]
        public ICollection<Users> Users { get; set; }
    }
}
