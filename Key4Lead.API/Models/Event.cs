﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("event")]
    public partial class Event
    {
        public Event()
        {
            FrmStruct = new HashSet<FrmStruct>();
            License = new HashSet<License>();
            StructEvent = new HashSet<StructEvent>();
        }

        [Column("eventId")]
        [StringLength(30)]
        public string EventId { get; set; }
        [Column("name")]
        [StringLength(100)]
        public string Name { get; set; }
        [Column("startDate", TypeName = "datetime")]
        public DateTime? StartDate { get; set; }
        [Column("endDate", TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
        [Column("city")]
        [StringLength(50)]
        public string City { get; set; }
        [Column("country")]
        [StringLength(50)]
        public string Country { get; set; }
        [Column("location")]
        [StringLength(100)]
        public string Location { get; set; }
        [Column("photoUrl")]
        [StringLength(100)]
        public string PhotoUrl { get; set; }
        [Column("barcodeType")]
        [StringLength(30)]
        public string BarcodeType { get; set; }

        [InverseProperty("Event")]
        public ICollection<FrmStruct> FrmStruct { get; set; }
        [InverseProperty("Event")]
        public ICollection<License> License { get; set; }
        [InverseProperty("Event")]
        public ICollection<StructEvent> StructEvent { get; set; }
    }
}
