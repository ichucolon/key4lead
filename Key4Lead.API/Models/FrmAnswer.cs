﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("frm_answer")]
    public partial class FrmAnswer
    {
        [Key]
        [Column("fId")]
        public long FId { get; set; }
        [Column("fstructId")]
        public int FstructId { get; set; }
        [Column("leadId")]
        public long LeadId { get; set; }
        [Column("answer")]
        public string Answer { get; set; }

        [ForeignKey("FstructId")]
        [InverseProperty("FrmAnswer")]
        public FrmStruct Fstruct { get; set; }
        [ForeignKey("LeadId")]
        [InverseProperty("FrmAnswer")]
        public Lead Lead { get; set; }
    }
}
