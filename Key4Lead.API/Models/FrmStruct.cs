﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("frm_struct")]
    public partial class FrmStruct
    {
        public FrmStruct()
        {
            FrmAnswer = new HashSet<FrmAnswer>();
        }

        [Key]
        [Column("fstructId")]
        public int FstructId { get; set; }
        [Required]
        [Column("eventId")]
        [StringLength(30)]
        public string EventId { get; set; }
        [Column("companyId")]
        public int CompanyId { get; set; }
        [Column("entry")]
        public string Entry { get; set; }

        [ForeignKey("CompanyId")]
        [InverseProperty("FrmStruct")]
        public Company Company { get; set; }
        [ForeignKey("EventId")]
        [InverseProperty("FrmStruct")]
        public Event Event { get; set; }
        [InverseProperty("Fstruct")]
        public ICollection<FrmAnswer> FrmAnswer { get; set; }
    }
}
