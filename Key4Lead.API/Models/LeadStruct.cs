﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("lead_struct")]
    public partial class LeadStruct
    {
        public LeadStruct()
        {
            Lead = new HashSet<Lead>();
            StructEvent = new HashSet<StructEvent>();
        }

        [Key]
        [Column("structId")]
        public int StructId { get; set; }
        [Column("entry")]
        public string Entry { get; set; }
        [Column("global")]
        public bool? Global { get; set; }

        [InverseProperty("Struct")]
        public ICollection<Lead> Lead { get; set; }
        [InverseProperty("Struct")]
        public ICollection<StructEvent> StructEvent { get; set; }
    }
}
