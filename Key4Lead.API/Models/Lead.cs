﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("lead")]
    public partial class Lead
    {
        public Lead()
        {
            FrmAnswer = new HashSet<FrmAnswer>();
        }

        [Column("leadId")]
        public long LeadId { get; set; }
        [Column("structId")]
        public int StructId { get; set; }
        [Column("answer")]
        public string Answer { get; set; }

        [ForeignKey("StructId")]
        [InverseProperty("Lead")]
        public LeadStruct Struct { get; set; }
        [InverseProperty("Lead")]
        public ICollection<FrmAnswer> FrmAnswer { get; set; }
    }
}
