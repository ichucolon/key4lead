﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Key4Lead.API.Models
{
    [Table("users")]
    public partial class Users
    {
        [Key]
        [Column("userId")]
        public int UserId { get; set; }
        [Column("username")]
        [StringLength(30)]
        public string Username { get; set; }
        [Column("fname")]
        [StringLength(50)]
        public string Fname { get; set; }
        [Column("lname")]
        [StringLength(50)]
        public string Lname { get; set; }
        [Column("city")]
        [StringLength(50)]
        public string City { get; set; }
        [Column("country")]
        [StringLength(50)]
        public string Country { get; set; }
        [Column("phone")]
        [StringLength(50)]
        public string Phone { get; set; }
        [Column("email")]
        [StringLength(30)]
        public string Email { get; set; }
        [Column("passwordHash")]
        public byte[] PasswordHash { get; set; }
        [Column("passwordSalt")]
        public byte[] PasswordSalt { get; set; }
        [Column("companyId")]
        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        [InverseProperty("Users")]
        public Company Company { get; set; }
    }
}
