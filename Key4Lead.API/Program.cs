﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Key4Lead.API.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;


namespace Key4Lead.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
            // var dbContext = new key4leadContext();
            // var records = dbContext.Event.Include(f => f.License).ThenInclude(r => r.Company).ToList();
            // foreach (var a in records) {
            //     System.Console.WriteLine($"Event: {a.Name}");
            //     foreach (var b in a.License){
            //         System.Console.WriteLine($"\tLicense: {b.LicenseKey}\tCompany: {b.Company.Name}");
            //     }
            //     System.Console.WriteLine("\n");
            // }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
