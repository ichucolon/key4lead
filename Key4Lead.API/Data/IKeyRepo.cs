using System.Collections.Generic;
using System.Threading.Tasks;
using Key4Lead.API.Models;

namespace Key4Lead.API.Data
{
    public interface IKeyRepo
    {
        Task<IEnumerable<Company>>GetLicenses();
        Task<License> GetLicense(string key);
    }
}