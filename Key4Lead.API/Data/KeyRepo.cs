using System.Collections.Generic;
using System.Threading.Tasks;
using Key4Lead.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Key4Lead.API.Data
{
    public class KeyRepo : IKeyRepo
    {
        private readonly key4leadContext _context;
        public KeyRepo(key4leadContext context)
        {
            _context = context;

        }
        public async Task<License> GetLicense(string key)
        {
            var liscense = await _context.License.Include(c => c.Company).Include(e => e.Event).FirstOrDefaultAsync(k => k.LicenseKey == key);

            return liscense;
        }

        public async Task<IEnumerable<Company>> GetLicenses()
        {
            var companies = await _context.Company.ToListAsync();
            return companies;
        }
    }
}